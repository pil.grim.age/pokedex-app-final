import globals from "globals";
import pluginJs from "@eslint/js";
import tseslint from "typescript-eslint";
import pluginReactConfig from "eslint-plugin-react/configs/recommended.js";
import { fixupConfigRules } from "@eslint/compat";
import prettierConfig from "eslint-config-prettier";
import prettierPlugin from "eslint-plugin-prettier";

export default [
  { files: ["**/*.{js,mjs,cjs,ts,jsx,tsx}"] },
  { languageOptions: { parserOptions: { ecmaFeatures: { jsx: true } } } }, // Включение поддержки JSX
  { languageOptions: { globals: globals.browser } }, // Определение глобальных переменных браузера
  pluginJs.configs.recommended, // Рекомендуемые правила для JavaScript
  ...tseslint.configs.recommended, // Рекомендуемые правила для TypeScript
  ...fixupConfigRules(pluginReactConfig), // Рекомендуемые правила для React
  prettierConfig, // Отключение конфликтующих правил с Prettier
  {
    plugins: {
      prettier: prettierPlugin, // Подключение плагина Prettier
    },
    rules: {
      // "prettier/prettier": "warn", // Использование Prettier для форматирования кода
      "react/react-in-jsx-scope": "off", // Отключение правила, требующего React в области видимости (не нужно с React 17+)
      "react/jsx-uses-react": "off", // Отключение правила, проверяющего использование React в JSX (не нужно с React 17+)
      "react/jsx-uses-vars": "warn", // Предотвращение удаления неиспользуемых переменных в JSX
      "react/prop-types": "off", // Отключение проверки PropTypes (если используете TypeScript)
      "@typescript-eslint/no-unused-vars": "off", // Ошибка при наличии неиспользуемых переменных в TypeScript
      "@typescript-eslint/explicit-module-boundary-types": "off", // Отключение требования явного указания типов на границах модулей
      "no-console": "warn", // Предупреждение при использовании console.log
      "no-debugger": "warn", // Предупреждение при использовании debugger
      "no-duplicate-imports": "warn", // Ошибка при наличии дублирующихся импортов
    },
  },
];
