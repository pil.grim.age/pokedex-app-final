import React from 'react';
import { createRoot } from 'react-dom/client';
import App from './view/App'; // Assuming App.js is in the same directory or specified correctly
import themeService from './service/themeService';

document.querySelector("body").classList.add(themeService.getTheme());
const rootElement = document.getElementById('root'); // Assuming 'root' is the id of your root element in HTML
const root = createRoot(rootElement);

root.render(
    <React.StrictMode>
        <App />
    </React.StrictMode>
);