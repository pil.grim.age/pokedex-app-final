

interface IPokemonStoreService{
    catch(id:number):void;
    check(id:number):boolean;
    getDate(id:number):string;
    getAll():number[];
}

export default IPokemonStoreService;