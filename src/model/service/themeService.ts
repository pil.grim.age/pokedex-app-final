

interface IThemeService{
    toggleTheme(): string;
    getTheme(): string;
}

export default IThemeService;