

interface IDictionary {
    [key: string]: { [key: string]: string };
}

export default IDictionary;