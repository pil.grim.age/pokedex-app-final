

interface IPokemonStore{
    catch(id:number, date:string):void;
    contains(id:number):boolean;
    get(id:number):string;
    getAll():number[];
}

export default IPokemonStore;