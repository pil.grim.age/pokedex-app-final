

interface IThemeStore{
    getTheme(): string;
    setTheme(theme: string): void;
    setDefaultTheme(defaultTheme: string): void;
}

export default IThemeStore;