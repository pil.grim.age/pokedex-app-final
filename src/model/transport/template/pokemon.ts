

interface IPokemon{
    id:number;
    name:string;
    sprites:{
        front_default:string;
    };
    types:Array<{
        type:{
            name:string;
        }
    }>
    abilities:Array<{
        ability:{
            name:string
        }
    }>
}

export default IPokemon;