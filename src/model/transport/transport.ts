import IPokemon from "./template/pokemon";
import IPokemons from "./template/pokemons";

interface IPokemonTransport{
    fetchPokemons(url:string, limit:number, offset:number):Promise<IPokemons>;
    fetchPokemon(url:string):Promise<IPokemon>;
    fetchPokemonById(id:string):Promise<IPokemon>;
}

export default IPokemonTransport;