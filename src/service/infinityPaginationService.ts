import IPaginationService from "@/model/service/paginationService";
import IPokemonService from "@/model/service/pokemonService";
import IPokemon from "@/model/transport/template/pokemon";
import IPokemons from "@/model/transport/template/pokemons";

class InfinityPaginationService implements IPaginationService{

    private service:IPokemonService;

    private nextPage:string;
    private prevPage:string;

    private currentPage:number;
    private totalPages:number;

    
    public constructor(transport:IPokemonService){
        this.currentPage = 0;
        this.service = transport;
    }

    getPage():number {
        return this.currentPage;
    }

    getTotalPage():number {
        return this.totalPages;
    }

    public async start():Promise<IPokemon[]>{
        this.currentPage++;
        const pokemons:IPokemons = await this.service.getPokemons();
        this.totalPages = Math.ceil(pokemons.count / pokemons.results.length);
        this.nextPage = pokemons.next;
        this.prevPage = pokemons.previous;
        return await this.service.getFormatedPokemons(pokemons);
    }

    public async next():Promise<IPokemon[]>{
        if (!this.nextPage){
            return [];
        }
        this.currentPage++;
        const pokemons:IPokemons = await this.service.getPokemons(this.nextPage);
        this.nextPage = pokemons.next;
        this.prevPage = pokemons.previous;
        return this.service.getFormatedPokemons(pokemons);
    }

    public async prev():Promise<IPokemon[]> {
        if (!this.prevPage){
            return [];
        }
        this.currentPage--;
        const pokemons:IPokemons = await this.service.getPokemons(this.prevPage);
        this.nextPage = pokemons.next;
        this.prevPage = pokemons.previous;
        return this.service.getFormatedPokemons(pokemons);
    }
}

export default InfinityPaginationService;
