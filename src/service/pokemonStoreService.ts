import IPokemonStoreService from "@/model/service/pokemonStoreService";
import IPokemonStore from "@/model/store/pokemonStore";
import pokemonStore from "@/store/pokemonStore";

class PokemonStoreService implements IPokemonStoreService{
    
    private static instance:IPokemonStoreService;
    private store:IPokemonStore;
    
    private constructor(store:IPokemonStore){
        this.store = store;
    }

    public static getInstance(store:IPokemonStore): IPokemonStoreService {
        if (!PokemonStoreService.instance) {
            PokemonStoreService.instance = new PokemonStoreService(store);
        }
        return PokemonStoreService.instance;
    }
    
    public catch(id: number): void {
        if(!this.check(id)){
            const now:Date = new Date();
            const date:string = `${now.getDate()}-${now.getMonth()+1}-${now.getFullYear()} ${now.getHours()}-${now.getMinutes()}-${now.getSeconds()}`
            
            this.store.catch(id, date);
        }
    }

    public check(id: number): boolean {
        return this.store.contains(id);
    }

    public getDate(id:number):string {
        return this.store.get(id);
    }

    public getAll():number[]{
        return  this.store.getAll();
    }
}

export default PokemonStoreService.getInstance(pokemonStore);