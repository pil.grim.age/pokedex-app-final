import IThemeService from "@/model/service/themeService";
import IThemeStore from "@/model/store/themeStore";
import themeStore from "@/store/themeStore";

class ThemeService implements IThemeService{
    
    private static instance: ThemeService;
    private themeStore: IThemeStore;
    private themes: string[];
    private currThemeNum:number;

    private constructor(themeStore:IThemeStore) {
        this.themes = ["light", "dark"]
        this.themeStore = themeStore;
        const theme =themeStore.getTheme();
        if (theme) {
            this.currThemeNum = this.themes.indexOf(theme);
        } else {
            this.currThemeNum = 0;
            themeStore.setDefaultTheme(this.themes[this.currThemeNum]);
        }
    }

    public static getInstance(themeStore:IThemeStore): ThemeService {
        if (!ThemeService.instance) {
            ThemeService.instance = new ThemeService(themeStore);
        }
        return ThemeService.instance;
    }

    public toggleTheme(): string {
        const newTheme = this.themes[++this.currThemeNum % this.themes.length];
        this.themeStore.setTheme(newTheme);
        return newTheme;
    }

    public getTheme(): string {
        return this.themeStore.getTheme();
    }
}

export default ThemeService.getInstance(themeStore);