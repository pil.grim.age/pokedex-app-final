import IThemeStore from "@/model/store/themeStore";
import { makeAutoObservable } from "mobx";

class ThemeStore implements IThemeStore{    
    private static instance: ThemeStore;

    public theme: string;

    private constructor() {
        this.loadFromLocalStorage();
        makeAutoObservable(this);
    }

    public static getInstance(): ThemeStore {
        if (!ThemeStore.instance) {
            ThemeStore.instance = new ThemeStore();
        }
        return ThemeStore.instance;
    }

    public getTheme(): string {
        return this.theme;
    }

    public setTheme(theme: string): void {
        this.theme = theme;
        this.saveToLocalStorage();
    }

    public setDefaultTheme(defaultTheme: string): void {
        if (!this.loadFromLocalStorage()){
            this.theme = defaultTheme;
        }
    }

    private saveToLocalStorage(): void {
        localStorage.setItem('theme', this.theme);
    }

    private loadFromLocalStorage(): boolean {
        const theme = localStorage.getItem('theme');
        if (theme) {
            this.theme = theme;
        }
        return !!theme;
    }
}

export default ThemeStore.getInstance();