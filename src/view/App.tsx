import { BrowserRouter, Routes, Route, Outlet, RouterProvider } from 'react-router-dom';
import './style/view.scss'
import router from './providers/router';
import langService from '@/service/langService';
import langStore from '@/store/langStore';

export default function App(){
    return(
        <RouterProvider router={router}/>
    );
}

