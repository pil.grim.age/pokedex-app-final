import React, { ReactNode } from "react";
import styles from "./styles/backButt.module.scss";
import { useNavigate } from "react-router-dom";

interface ButtonProps{
    children?: React.ReactNode;
}

export default function BackButt({children, ...props }:ButtonProps){
    const nav = useNavigate();

    const back = ()=>nav(-1);

    return(
        <button onClick={back} className={styles.button}>
            <div className={styles.button__arrow}>
            </div>
        </button>
    )
}