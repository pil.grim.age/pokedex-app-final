import styles from "./styles/heading.module.scss"

interface HeadingProps{
    children: React.ReactNode;
}

export default function Button({children, ...props }:HeadingProps){
    return(
        <h1 className={styles.heading} {...props}>
            {children}
        </h1>
    )
}