import styles from "./styles/toolButt.module.scss"


interface ToolButtProps{
    name:string;
    className:string;
    event:React.MouseEventHandler<HTMLButtonElement>;
}

export default function ToolButt({name, className, event, ...props}:ToolButtProps){
    return(
        <button className={styles.button} title={name} onClick={event} {...props}>
            <div className={`${styles.button__icon} ${className}`} title={name} />
        </button>
    );
}