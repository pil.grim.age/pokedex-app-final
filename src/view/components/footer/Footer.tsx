import langStore from "@/store/langStore";
import styles from "./sryles/footer.module.scss"
import { observer } from "mobx-react-lite";

const Footer = observer(() => {
    return(
        <footer className={styles.footer}>
            <div className={styles.footer__container}>
                <span className={styles.copywrite}>
                    {langStore.dictionary[langStore.getlang()].footer}
                </span>
                <span className={styles.copywrite}>
                    {langStore.dictionary[langStore.getlang()].summer} 2024
                </span>
            </div>
        </footer>
    );
});

export default Footer;