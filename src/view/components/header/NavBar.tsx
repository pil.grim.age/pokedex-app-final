import { Link, NavLink, Route } from "react-router-dom";
import styles from "./styles/navBar.module.scss";
import langStore from "@/store/langStore";
import { observer } from "mobx-react-lite";

const setActive = (({isActive}:{isActive:boolean;}) => isActive ? styles.active : "");

const NavBar = observer(() => {
    return(
        <nav className={styles.navBar}>
            <NavLink to="/pokemons" className={setActive}>
                {langStore.dictionary[langStore.getlang()].pokemons}
            </NavLink>
            <NavLink to="/my-pokemons" className={setActive}>
                {langStore.dictionary[langStore.getlang()].myPokemons}
            </NavLink>
        </nav>
    );
});

export default NavBar;