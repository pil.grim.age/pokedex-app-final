import { Link, NavLink } from "react-router-dom";
import styles from "./styles/title.module.scss"
import { observer } from "mobx-react-lite";
import langStore from "@/store/langStore";

const Title = observer(() => {
    
    return(
        <div className={styles.wrapper}>
            <h1 className={styles.title}>
                <NavLink to="/home">
                    {langStore.dictionary[langStore.getlang()].pokedex}
                </NavLink>
                <span className={styles.sub}>
                    {langStore.dictionary[langStore.getlang()].catchThePokemons}
                </span>
            </h1>
        </div>
    );
})
export default Title;