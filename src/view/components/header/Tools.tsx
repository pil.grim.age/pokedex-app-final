import themeService from "@/service/themeService";
import ToolButt from "../UI/ToolButt";
import styles from "./styles/tools.module.scss";
import langStore from "@/store/langStore";
import { useState } from "react";
import langService from "@/service/langService";
import { observer } from "mobx-react-lite";

const Tools = observer(() => {
    
    const toggleLanguage = () => {
        langService.toggleLang();
    }

    const toggleTheme = () => {
        const root = document.querySelector("body");
        root.classList.remove(themeService.getTheme());
        root.classList.add(themeService.toggleTheme());
    }

    return(
        <div className={styles.tools}>
            <ToolButt 
                name="lang" 
                className={
                    `${styles.lang} ${langStore.lang==="ru" ? styles.langRu : styles.langEn}`
                    
                } 
                event={toggleLanguage} 
            />
            <ToolButt 
                name="theme" 
                className={styles.theme} 
                event={toggleTheme}
            />
        </div>
    );
});

export default Tools;