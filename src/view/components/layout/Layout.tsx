import { Outlet } from "react-router-dom";
import Footer from "../footer/Footer";
import Header from "../header/Header";
import { observer } from "mobx-react-lite";

const Layout = observer(() => {
        return(
            <>
                <Header/>
                <main>
                    <Outlet/>
                </main>
                <Footer/>
            </>
        );
    }
);
export default Layout;