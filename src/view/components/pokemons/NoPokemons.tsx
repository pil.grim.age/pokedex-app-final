import langStore from "@/store/langStore";
import Heading from "../UI/Heading";
import { observer } from "mobx-react-lite";

const NoPokemons = observer(() => {
    return(
        <Heading>
            {langStore.dictionary[langStore.getlang()].noPokemons}
        </Heading>
    );
});

export default NoPokemons;