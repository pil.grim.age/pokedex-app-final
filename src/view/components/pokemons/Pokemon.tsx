import { Link } from "react-router-dom";
import IPokemon from "@/model/transport/template/pokemon";
import store from "@/service/pokemonStoreService";
import styles from "./styles/pokemon.module.scss";
import { useState } from "react";
import getNeonBorderStyles from "./styles/getNeonBorderStyles";
import langStore from "@/store/langStore";
import { observer } from "mobx-react-lite";


interface PokemonProps{
    pokemon:IPokemon;
    catchBack?:()=>void;
}

const Pokemon = observer(({pokemon, catchBack}:PokemonProps) => {
    
    const style = getNeonBorderStyles([...pokemon.types.map((item)=>(item.type.name))])
    const [isCaught, setIsCaught] = useState(store.check(pokemon.id));

    const catchPokemon = ()=>{
        store.catch(pokemon.id);
        setIsCaught(true);
        if(catchBack){
            catchBack();
        }
    }

    return(
        <div className={styles.pokemon} >
            <div className={styles.pokemon__imgContainer} >
                <Link  to={`/pokemon/${pokemon.id}`}>
                    <img  
                        className={styles.pokemon__img} 
                        src={pokemon.sprites.front_default} 
                        alt={pokemon.id.toString()} 
                    />
                </Link>
            </div>

            <div >
                {pokemon.name}
            </div>

            <div style={style}>
                {pokemon.id}
            </div>
        
            {
                <button  
                    className={`${styles.pokemon__button} ${isCaught ? styles.caught : ""}`}
                    onClick={catchPokemon}
                >
                    {isCaught ? langStore.dictionary[langStore.getlang()].caught : langStore.dictionary[langStore.getlang()].catch}
                </button>
            }
        </div>
    );
});

export default Pokemon;