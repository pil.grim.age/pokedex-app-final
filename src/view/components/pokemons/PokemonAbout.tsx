import { useEffect, useState } from "react";
import styles from "./styles/pokemonDeck.module.scss"
import useFetching from "@/view/hooks/useFetching";
import Loader from "./../UI/Loader";
import IPokemon from "@/model/transport/template/pokemon";
import DeckSpace from "../UI/DeckSpace";
import Heading from "../UI/Heading";
import NoPokemons from "./NoPokemons";
import service from "@/service/pokemonService";
import Pokemon from "./Pokemon";
import store from "@/service/pokemonStoreService";
import PokemonProperties from "./PokemonProperties";
import stylesabout from "./styles/pokemonAbout.module.scss"
import langStore from "@/store/langStore";
import { observer } from "mobx-react-lite";

interface PokemonAboutProps{
    id:string;
}

const PokemonAbout = observer(({id}:PokemonAboutProps) => {
    const [pokemon, setPokemon] = useState(null);
    const [date, setDate] = useState([false,""]);
    
    const [fetching, isLoading, error] = useFetching(fetchPokemon);

    useEffect(() => {
        fetching();
        if (store.check(parseInt(id))){
            catchPokemon();
        }
    }, []);

    async function fetchPokemon():Promise<void>{
        const newPokemon:IPokemon = await service.getPokemonById(id);
        setPokemon(newPokemon);
    }

    function catchPokemon(){
        setDate([true, store.getDate(parseInt(id))]);
    }

    return(
        <div className={`${styles.pokemonDeck} ${stylesabout.about}`}>
            {
                pokemon ? <><PokemonProperties pokemon={pokemon}/>  
                <Pokemon pokemon={pokemon} catchBack={catchPokemon} /></> : <></>
            }
            
            <div className={stylesabout.paper}>
                <DeckSpace>
                    {
                        isLoading && <Loader/> 
                        || 
                        (!pokemon) && <NoPokemons/> 
                        || 
                        <Heading>
                            {
                                date[0] 
                                ? langStore.dictionary[langStore.getlang()].caught
                                : langStore.dictionary[langStore.getlang()].notCaught
                            }

                            <br/>
                            <span>
                                {date[1]}
                            </span>
                        </Heading>
                    }
                </DeckSpace>
            </div>
        </div>
    );
});


export default PokemonAbout;