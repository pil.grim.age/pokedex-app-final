import { useEffect, useRef, useState } from "react";
import PokemonList from "./PokemonList";
import styles from "./styles/pokemonDeck.module.scss";
import useFetching from "@/view/hooks/useFetching";
import Loader from "./../UI/Loader";
import InfinityPaginationService from "@/service/infinityPaginationService";
import { useObserver } from "@/view/hooks/useObserver";
import IPokemon from "@/model/transport/template/pokemon";
import DeckSpace from "../UI/DeckSpace";
import Heading from "../UI/Heading";
import NoPokemons from "./NoPokemons";
import pokemonService from "@/service/pokemonService";
import storeService from "@/service/pokemonStoreService";
import { observer } from "mobx-react-lite";
import langStore from "@/store/langStore";
interface PokemonDeckProps {
  my?: boolean;
  filter?: (pokemons:IPokemon[]) => IPokemon[];
}

const PokemonDeck = observer(({ my, filter }: PokemonDeckProps) => {
  const [service, _] = useState(new InfinityPaginationService(pokemonService));
  const [pokemons, setPokemons] = useState([]);
  const [pages, setPages] = useState(0);
  const lastElement = useRef();

  const [fetching, isLoading, error] = useFetching(
    my ? getMyPokemons : fetchPokemons,
  );

  useObserver({
    ref: lastElement,
    canLoad: service.getPage() <= service.getTotalPage(),
    isLoading,
    callback: () => {
      setPages((pages) => pages + 1);
    },
  });

  useEffect(() => {
    fetching(!pages);
  }, [pages]);

  async function getMyPokemons() {
    const promices: Promise<IPokemon>[] = storeService
      .getAll()
      .sort((a,b)=>(a-b))
      .map(async (key) => await pokemonService.getPokemonById(key.toString()));
    const newPokemons: IPokemon[] = await Promise.all(promices);
    setPokemons([...(filter ? filter(newPokemons) : newPokemons)]);
  }

  async function fetchPokemons(first: boolean = false): Promise<void> {
    const newPokemons: IPokemon[] = first
      ? await service.start()
      : await service.next();
    setPokemons([...pokemons, ...(filter ? filter(newPokemons) : newPokemons)]);
  }

  return (
    <div className={styles.pokemonDeck}>
      <PokemonList pokemons={pokemons} isLoading={isLoading} />

      <DeckSpace>
        {(isLoading && <Loader />) ||
          (!pokemons.length && <NoPokemons />) ||
          (service.getPage() >= service.getTotalPage() && (
            <Heading>
              {langStore.dictionary[langStore.getlang()].thatsAll}
            </Heading>
          )) ||
          (!!my && <Heading>{pokemons.length}</Heading>)}
      </DeckSpace>

      <div ref={lastElement}></div>
    </div>
  );
});

export default PokemonDeck;
