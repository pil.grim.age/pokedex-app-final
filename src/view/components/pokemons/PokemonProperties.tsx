import IPokemon from "@/model/transport/template/pokemon";
import styles from "./styles/pokemonProperties.module.scss"
import styles2 from "./styles/pokemon.module.scss"
import { typeColors } from "./styles/getNeonBorderStyles";
import langStore from "@/store/langStore";
import { observer } from "mobx-react-lite";

interface PokemonPropertiesProps{
    pokemon:IPokemon;
}


const PokemonProperties = observer(({pokemon}:PokemonPropertiesProps) => {
    const mainClass: string = `${styles2.pokemon} ${styles.properties} ${styles.cardProperties}`;

    return(
        <div className={mainClass}>
            <h4 className={styles.heading}>
                {langStore.dictionary[langStore.getlang()].name}: 
                <span>
                    {pokemon.name}
                </span>
            </h4>
            <h4 className={styles.heading}>
                ID: 
                <span>
                    {pokemon.id}
                </span>
            </h4>
            <h4 className={styles.heading}>
                {langStore.dictionary[langStore.getlang()].types}:
            </h4>
            <ul className={styles.list}>
                {pokemon.types.map((typeInfo, index) => {
                    return <li key={index} style={{color:`${typeColors[typeInfo.type.name]}`}}>{typeInfo.type.name}</li>
                })}
            </ul>
            <h4 className={styles.heading}>
                {langStore.dictionary[langStore.getlang()].abilities}:
            </h4>
            <ul className={styles.list}>
                {pokemon.abilities.map((abilityInfo, index) => (
                    <li key={index}>{abilityInfo.ability.name}</li>
                ))}
            </ul>
        </div>
    )
});

export default PokemonProperties;