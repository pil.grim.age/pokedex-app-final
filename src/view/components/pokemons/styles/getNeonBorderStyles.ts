export const typeColors: { [key: string]: string } = {
    'normal': '#75515B',
    'fighting': '#994025',
    'flying': '#4A677D',
    'poison': '#5D2D87',
    'ground': '#A9702C',

    'rock': '#48180B',
    'bug': '#1C4B27',
    'ghost': '#33336B',
    'steel': '#5F756D',
    'fire': '#AB1F23',

    'water': '#1552E2',
    'grass': '#147B3D',
    'electric': '#E3E32B',
    'psychic': '#A42A6C',
    'ice': '#86D2F5',

    'dragon': '#448B95',
    'dark': '#040706',
    'fairy': '#971944',
    'stellar': '#ffffff',
    'unknown': 'transparent',

    'shadow': '#A7A9AD',
    // добавьте другие способности и цвета по мере необходимости
  };
  
  
  export default function getNeonBorderStyles(types: string[]): React.CSSProperties{
    if (types.length === 0 || types.length > 2) {
      throw new Error('Abilities array must contain 1 or 2 items');
    }
  
    const color1 = typeColors[types[0]] || '#000'; // цвет для первой способности
    const color2 = types[1] ? typeColors[types[1]] || '#000' : color1; // цвет для второй способности
  
    return {
      
      minWidth:'20px',
      padding:"6px",
      borderRadius: '30px',
      borderStyle: "dashed",
      borderWidth: '3px',
      borderColor: `${color1} ${color2} ${color2} ${color1}`,
      // boxShadow: '0 0 3px black, inset 0 0 3px black',
    //   boxShadow: `-1px -1px 10px ${color1}, 1px 1px 10px ${color2}, inset -1px -1px 10px ${color2}, inset 1px 1px 10px ${color1}`
    };
  };