import langStore from "@/store/langStore";
import BackButt from "../components/UI/BackButt";
import DeckSpace from "../components/UI/DeckSpace";
import Heading from "../components/UI/Heading";
import styles from "./../components/pokemons/styles/pokemonDeck.module.scss"
import { observer } from "mobx-react-lite";

const Error = observer(() => {
    return(
        <>
            <BackButt/>
            <div className={styles.pokemonDeck}>
                <DeckSpace>
                    <Heading>
                        {langStore.dictionary[langStore.getlang()].error404}
                    </Heading>
                </DeckSpace>
            </div>
        </>
        
    );
});

export default Error;