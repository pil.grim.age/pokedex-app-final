import langStore from "@/store/langStore";
import DeckSpace from "../components/UI/DeckSpace";
import Heading from "../components/UI/Heading";
import styles from "./../components/pokemons/styles/pokemonDeck.module.scss"
import { observer } from "mobx-react-lite";

const Home = observer(() => {
        return(
            <div className={styles.pokemonDeck}>
                <DeckSpace>
                    <Heading>
                        {langStore.dictionary[langStore.getlang()].hello}
                    </Heading>
                </DeckSpace>
            </div>
        );
    }
);

export default Home;