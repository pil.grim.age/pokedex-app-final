import IPokemonFormated from "@/model/store/pokemonFormated";
import PokemonDeck from "../components/pokemons/PokemonDeck";
import pokemonStoreService from "@/service/pokemonStoreService";

export default function MyPokemons(){
    const myPokemons = (pokemons:IPokemonFormated[]):IPokemonFormated[] => {
        const service = pokemonStoreService;
        return pokemons.filter(item=>service.check(item.id));
    }

    return(
        <PokemonDeck my={true} />
    );
}