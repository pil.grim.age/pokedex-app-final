import { Navigate, Route, createBrowserRouter, createHashRouter, createRoutesFromElements } from "react-router-dom";
import Home from "../pages/Home";
import Layout from "../components/layout/Layout";
import AllPokemons from "../pages/AllPokemons";
import MyPokemons from "../pages/MyPokemons";
import PokemonPage from "../pages/PokemonPage";
import Error from "../pages/Error";


const router = createHashRouter(createRoutesFromElements(
    <Route path="/" element={<Layout />}>
        <Route index element={<Navigate to="/pokemons" replace />}/>
        <Route path="home" element={<Home />}/>
        <Route path="my-pokemons" element={<MyPokemons />}/>
        <Route path="pokemons" element={<AllPokemons />}/>
        <Route path="pokemon/:id" element={<PokemonPage />}/>
        <Route path="*" element={<Error />}></Route>
    </Route>
));

export default router;